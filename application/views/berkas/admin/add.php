<div class="row">
	<div class="col-sm-12 animated bounceInRight">
		<form method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-9">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title"><?= ucwords($global->headline)?></h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-12">	
									<div class="form-group">
										<label>Nama Berkas</label>
										<input required type="text" name="berkas_nama" class="form-control text-capitalize">
									</div>						
									<div class="form-group">
										<label>Keterangan</label>
										<textarea required class="form-control" id="editor1" name="berkas_keterangan"></textarea>
									</div>																				
								</div>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title"><span class="fa fa-gears"></span> Pengaturan</h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Id</label>
										<input type="text" name="id" placeholder="Auto Generated" readonly class="form-control">
									</div>
									<div class="form-group">
										<label>User</label>
										<input type="text" class="form-control" value="<?= $this->session->userdata('user_nama')?>" readonly="readonly">
										<input type="text" name="berkas_iduser" class="hide form-control" value="<?= $this->session->userdata('user_id')?>" readonly="readonly">										
									</div>									
									<div class="form-group">
										<label>Tanggal Tersimpan</label>
										<input type="text" name="berkas_date" class="form-control" value="<?= date('d-m-Y')?>" readonly="readonly">
									</div>																			
									<div class="form-group">
										<label>Kategori</label>
										<select required class="form-control selectdata" name="berkas_idkategori" style="width:100%">
											<option value="" disabled="disabled" selected="selected">Pilih Kategori</option>
											<?php foreach($kategori AS $row):?>
												<option value="<?=$row->kategori_id?>?"><?=ucwords($row->kategori_nama)?></option>
											<?php endforeach;?>
										</select>
									</div>
									<div class="form-group">
										<label>Status</label>
										<select class="form-group selectdata" name="berkas_status" style="width:100%">
											<option value="1">Aktif</option>
											<option value="0">Tidak Aktif</option>
										</select>
									</div>
									<div class="form-group">
										<label>File Berkas</label>
										<input required type="file" name="fileupload">
										<p class="help-block">Ukuran maksimal 5mb, pdf</p>
									</div>																			
									<div class="form-group">
										<button type="submit" value="submit" name="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
									</div>																																																												 
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</form>			
	</div>	
</div>
<script type="text/javascript">
	CKEDITOR.replace('editor1',{
		height:300,
	});
</script>
<?php include 'action.php';?>