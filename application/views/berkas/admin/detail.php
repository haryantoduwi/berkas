<div class="modal fade" id="detail">
  <div class="modal-dialog" style="min-width: 90%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= ucwords($global->headline)?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <a href="<?= base_url($global->url.'download/'.$data->berkas_file)?>" class="pull-right btn btn-flat btn-md btn-primary">download <i class="fa fa-download"></i></a>
        </div>
        <div class="form-group">
          <embed src="<?= base_url($path.$data->berkas_file)?>" style="width: 100%;height: 500px; margin-top: 10px">
            
          </embed>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>