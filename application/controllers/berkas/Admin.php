<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'controllers/Master.php';	
class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		$this->userlevel=$this->session->userdata('user_level');
		$this->cekuseradmin();
	}
	//VARIABEL
	private $master_tabel="berkas";
	private $default_url="berkas/admin/";
	private $default_view="berkas/admin/";
	private $view="template/backend";
	private $id="berkas_id";
	private $path='./upload/berkas/';

	private function global_set($data){
		$data=array(
			'menu'=>'berkas',
			'submenu_menu'=>'berkas',
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-tags",
			'view'=>"views/berkas/admin/index.php",
			'detail'=>true,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data;
	}
	private function uploadform(){
		$file='fileupload';
		if($_FILES[$file]['name']){
			if($this->fileupload($this->path,$file)){
				$fileupload=$this->upload->data('file_name');
				return $fileupload;	
			}else{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				redirect(site_url($this->default_url));
			}
		}
			
	}	
	private function get_berkas(){
		$query=array(
			'select'=>'a.*,b.kategori_nama,c.user_nama',
			'tabel'=>'berkas a',
			'join'=>array(array('tabel'=>'kategori b','ON'=>'b.kategori_id=a.berkas_idkategori','jenis'=>'INNER'),
				array('tabel'=>'user c','ON'=>'c.user_id=a.berkas_iduser','jenis'=>'INNER')
			),
			'order'=>array('kolom'=>'a.berkas_date','orderby'=>'DESC'),
		);	
		$result=$this->Crud->join($query)->result();	
		return $result;
	}		

	public function index()
	{
		$global_set=array(
			'submenu'=>false,
			'headline'=>'berkas',
			'url'=>'berkas/admin/',
		);
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'berkas_nama'=>$this->input->post('berkas_nama'),
				'berkas_keterangan'=>$this->input->post('berkas_keterangan'),
				'berkas_iduser'=>$this->input->post('berkas_iduser'),
				'berkas_idkategori'=>$this->input->post('berkas_idkategori'),
				'berkas_status'=>$this->input->post('berkas_status'),
				'berkas_date'=>date('Y-m-d',strtotime($this->input->post('berkas_date'))),
			);
			$data['berkas_file']=$this->uploadform();
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu($this->userlevel),
			);			
			$this->load->view($this->view,$data);
			//$this->dumpdata($data);
		}
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'berkas/admin/',
		);
		$global=$this->global_set($global_set);
		$query=array(
			'tabel'=>$this->master_tabel,
		);			
		//PROSES TAMPIL DATA
		$data=array(
			'global'=>$global,
			'data'=>$this->get_berkas(),
		);
		$this->load->view($this->default_view.'tabel',$data);		
		//$this->dumpdata($data);
	}
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'berkas',
			'url'=>'berkas/admin/', //AKAN DIREDIRECT KE INDEX
		);
		$global=$this->global_set($global_set);
		$kategori=array(
			'tabel'=>'kategori',
			'order'=>array('kolom'=>'kategori_id','orderby'=>'ASC'),
		);		
		$data=array(
			'kategori'=>$this->Crud->read($kategori)->result(),
			'global'=>$global,
			);
		$this->load->view($this->default_view.'add',$data);		
	}	
	public function edit(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'edit data',
			'url'=>'berkas/admin/edit',
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'berkas_nama'=>$this->input->post('berkas_nama'),
				'berkas_keterangan'=>$this->input->post('berkas_keterangan'),
				'berkas_iduser'=>$this->input->post('berkas_iduser'),
				'berkas_idkategori'=>$this->input->post('berkas_idkategori'),
				'berkas_status'=>$this->input->post('berkas_status'),
				'berkas_date'=>date('Y-m-d',strtotime($this->input->post('berkas_date'))),
			);
			$upload=$this->uploadform();
			if($upload){
				//AMBIL FILLAMA DAN KEMUDIAN HAPUS
				$filelama=array(
					'tabel'=>$this->master_tabel,
					'kolomid'=>$this->id,
					'valueid'=>$id,
					'kolomfile'=>'berkas_file',
					'path'=>$this->path,
				);
				$this->hapusfileupload($filelama);				
				$data['post_featuredimage']=$upload;			
			}
			$query=array(
				'data'=>$data,
				'where'=>array($this->id=>$id),
				'tabel'=>$this->master_tabel,
			);			
			$update=$this->Crud->update($query);
			$this->notifiaksi($update);
			redirect(site_url($this->default_url));
			//$this->dumpdata($id);
		}else{
			$kategori=array(
				'tabel'=>'kategori',
				'order'=>array('kolom'=>'kategori_id','orderby'=>'ASC'),
			);				
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->id=>$id)),
			);					
			$data=array(
				'kategori'=>$this->Crud->read($kategori)->result(),
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
			);
			$this->load->view($this->default_view.'edit',$data);
			//$this->dumpdata($data);
		}
	}	
	public function hapus($id){
		$data=array(
			'tabel'=>$this->master_tabel,
			'kolomid'=>$this->id,
			'valueid'=>$id,
			'kolomfile'=>'berkas_file',
			'path'=>$this->path,
		);
		$this->hapusfileupload($data);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->id=>$id),
		);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}
	public function download($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail berkas',
			'url'=>'berkas/admin/',
		);
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'tabel'=>'berkas',
			'where'=>array(array('berkas_id'=>$id)),
		);
		$data=array(
			'data'=>$this->Crud->read($query)->row(),
			'path'=>$this->path,
			'global'=>$global,
		);
		$this->load->view($this->default_view.'detail',$data);		
		//$this->dumpdata($data);
	}	
}
