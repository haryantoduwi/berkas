<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'controllers/Master.php';	
class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		$this->userlevel=$this->session->userdata('user_level');
		$this->cekuseradmin();

	}
	//VARIABEL
	private $master_tabel="berkas";
	private $default_url="dashboard/Admin/";
	private $default_view="dashboard/Admin/";
	private $view="template/backend";
	private $id="berkas_id";

	private function global_set($data){
		$data=array(
			'menu'=>'dashboard',
			'submenu_menu'=>false,
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-tasks",
			'view'=>"views/dashboard/admin/index.php",
			'detail'=>true,
			'edit'=>false,
			'delete'=>false,
		);
		return (object)$data;
	}		
	public function index()
	{

		$global_set=array(
			'submenu'=>false,
			'headline'=>'dashboard',
			'url'=>'dashboard/admin/',
		);
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'nama'=>$this->input->post('nama'),
				'tgllahir'=>date('Y-m-d',strtotime($this->input->post('tgllahir'))),
				'nomerhp'=>$this->input->post('nomerhp'),
				'desa'=>$this->input->post('desa'),
			);
			$file='fileupload';
			if($_FILES[$file]['name']){
				if($this->fileupload($this->path,$file)){
					$file=$this->upload->data('file_name');
					$data['pendaftaran_file']=$file;
					//print_r($data);
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array('status'=>'lulus')
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data);
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu($this->userlevel),
			);			
			$this->load->view($this->view,$data);
			//$this->dumpdata($data);
		}
	}
	private function get_data($data){
		$query=array(
			'tabel'=>$data['tabel'],
			'order'=>array('kolom'=>$data['id'],'orderby'=>'DESC'),
			'limit'=>$data['limit'],
		);		
		return $query;
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'dashboard/admin/',
		);
		$global=$this->global_set($global_set);
		$berkas=array(
			'tabel'=>$this->master_tabel,
			'id'=>$this->id,
			'limit'=>10,
		);
		$kategori=array(
			'tabel'=>'kategori',
			'id'=>'kategori_id',
			'limit'=>0,
		);
		$user=array(
			'tabel'=>'user',
			'id'=>'user_id',
			'limit'=>10,
		);				
		//PROSES TAMPIL DATA
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($this->get_data($berkas))->result(),
			'kategori'=>$this->Crud->read($this->get_data($kategori))->result(),
			'user'=>$this->Crud->read($this->get_data($user))->result(),
		);
		$this->load->view($this->default_view.'tabel',$data);		
		//$this->dumpdata($data);
	}
}
